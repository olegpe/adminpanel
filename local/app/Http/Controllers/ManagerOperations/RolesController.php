<?php

namespace App\Http\Controllers\ManagerOperations;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Auth;
class RolesController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);//isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    public function index()
    {
        $roles = Role::all();

        return view("role.index")->with(['roles'=>$roles]);
    }

    public function create()
    {
        $permissions = Permission::pluck('name', 'id')->toArray();
        return view('role.create')->with(['permissions' => $permissions]);
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'name' => 'required|max:255|unique:roles,name',
            'permission' => 'required',

        ]);

        $role = Role::create(['name' => $request->input('name')]);
        foreach ($request->input('permission') as $id){
            $permission = Permission::findOrFail($id);
            $role->givePermissionTo($permission);
        }


        return redirect()->route('role.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::pluck('name', 'id')->toArray();
        return view('role.edit')->with(['permissions' => $permissions, 'role' => $role]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'name' => 'required|max:255|unique:roles,name,'.$id,
            'permission' => 'required',

        ]);


        $role = Role::findOrFail($id);

        $oldPermissions = $role->permissions;
        foreach ($oldPermissions as $oldPermission){
            $role->revokePermissionTo($oldPermission);
        }
      //  dd($oldPermission->sync($request->input('permission')));
        foreach ($request->input('permission') as $id){
            $permission = Permission::findOrFail($id);
          $role->givePermissionTo($permission);
        }
        return redirect()->route('role.index');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('role.index');

    }
}
