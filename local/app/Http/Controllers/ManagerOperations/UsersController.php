<?php

namespace App\Http\Controllers\ManagerOperations;

use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view("user.index")->with(['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id')->toArray();
        return view('user.create')->with(['roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'color' => 'required|string',
            'roles' => 'required',
        ]);
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->color = $request->input('color');

        $user->save();
        foreach ($request->input('roles') as $id){
            $role = Role::findOrFail($id);
            $user->assignRole($role);
        }
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::pluck('name', 'id')->toArray();
        $userRole = $user->roles()->pluck('id');
       // dd($userRole);
        return view('user.edit')->with(['user'=>$user, 'roles' => $roles, 'userRole' => $userRole]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
        'name' => 'required|string|max:30',
        'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
        'password' => 'string|min:6|confirmed',
        'color' => 'required|string',
        'roles' => 'required',

        ]);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->color = $request['color'];
        if($request['password'])
            $user->password = bcrypt($request['password']);

        $oldRoles = $user->roles;
        foreach ($oldRoles as $role){
            $user->removeRole($role->name);
        }

        foreach ($request['roles'] as $id){
            $role = Role::findOrFail($id);
            $user->assignRole($role->name);
        }

        $user->save();

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
