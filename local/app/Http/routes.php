<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::auth();

Route::get('/', 'HomeController@index');
Route::get('managers', 'ManagerOperations\UsersController@index')->name('user.index');
Route::get('managers/create', 'ManagerOperations\UsersController@create')->name('user.create');
Route::post('managers/create', 'ManagerOperations\UsersController@store')->name('user.store');
Route::get('managers/edit/{id}', 'ManagerOperations\UsersController@edit')->name('user.edit');
Route::post('managers/edit/{id}', 'ManagerOperations\UsersController@update')->name('user.update');
Route::delete('managers/delete/{id}', 'ManagerOperations\UsersController@destroy')->name('user.destroy');

Route::get('role/', 'ManagerOperations\RolesController@index')->name('role.index');
Route::get('role/create', 'ManagerOperations\RolesController@create')->name('role.create');
Route::post('role/create', 'ManagerOperations\RolesController@store')->name('role.store');
Route::get('role/edit/{id}', 'ManagerOperations\RolesController@edit')->name('role.edit');
Route::post('role/edit/{id}', 'ManagerOperations\RolesController@update')->name('role.update');
Route::delete('role/delete/{id}', 'ManagerOperations\RolesController@destroy')->name('role.destroy');
