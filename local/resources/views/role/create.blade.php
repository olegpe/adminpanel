@extends('layouts.main')
@section("css")

    <link href="{{ asset('css/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/css/font-awesome.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/skin-blue.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

@endsection
@section("content")
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Role</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                            <form class="form-horizontal" role="form" method="POST" action="{{route('role.store')}}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Name</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                                    <label for="permission" class="col-md-4 control-label">Permissions</label>

                                    <div class="col-md-6">
                                        <select name="permission[]" class="form-control select2" multiple="multiple" data-placeholder="Select a State"
                                                style="width: 100%;">
                                            @foreach($permissions as $key => $permission)
                                                <option value="{{$key}}">{{$permission}}</option>

                                            @endforeach
                                        </select>
                                        @if ($errors->has('permission'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('permission') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn  btn-primary">
                                            <i class="fa fa-btn fa-key"></i> Create
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <script src="{{ asset('js/select2.full.min.js') }}"></script>
    <script>
        $(function(){
            $('.select2').select2()
        });
    </script>
@endsection