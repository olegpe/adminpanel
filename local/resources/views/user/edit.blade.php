@extends('layouts.main')
@section("css")

    <link href="{{ asset('css/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/css/font-awesome.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/skin-blue.min.css') }}" rel="stylesheet" />
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

@endsection
@section("content")
    <div class="register-box">

        <div class="register-box-body">
            <p class="login-box-msg">Update {{$user->name}}</p>

            <form action="{{route('user.update', $user->id)}}" method="post">
                {{ csrf_field() }}
                <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="Full name">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password" placeholder="New password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                    @endif
                </div>
                <div class="form-group has-feedback{{ $errors->has('color') ? ' has-error' : '' }}">
                    <input id="color" type="text" class="form-control" name="color" value="{{$user->color}}" />
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('color'))
                        <span class="help-block">
                                                    <strong>{{ $errors->first('color') }}</strong>
                                                </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">

                    <select name="roles[]" class="form-control select2" multiple="multiple" data-placeholder="Select a State"
                            style="width: 100%;">
                        @foreach($roles as $key => $role)
                            <option value="{{$key}}">{{$role}}</option>

                        @endforeach
                    </select>
                    @if ($errors->has('roles'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>


        </div>
        <!-- /.form-box -->
    </div>

@endsection

@section("script")
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <script src="{{ asset('js/select2.full.min.js') }}"></script>

    <script src="{{ asset('js/bootstrap-colorpicker.min.js') }}"></script>
    <script>
        $(document).ready(function(){

                $('.select2').select2();
                var array = [];
                 @foreach($userRole as $role)
                array.push('{{$role}}');
                @endforeach
                console.log(array);

                $(".select2").val(array).trigger("change");


            var color;
            $('#color').css('background-color', '{{$user->color}}');
            color = $('#color').colorpicker().on('changeColor', function(ev){
                $('#color').css('background-color', ev.color.toHex());
                $('#color').val(ev.color.toHex());
            });
        });
    </script>
@endsection