<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      /*  Permission::create(['name' => 'Admin']);
        Permission::create(['name' => 'create order']);
        Permission::create(['name' => 'edit order']);
        Permission::create(['name' => 'delete order']);

        Permission::create(['name' => 'create prodct']);
        Permission::create(['name' => 'delete prodct']);
        Permission::create(['name' => 'edit prodct']); */

        Permission::create(['name' => 'create category']);
        Permission::create(['name' => 'delete category']);
        Permission::create(['name' => 'edit category']);
    }
}
