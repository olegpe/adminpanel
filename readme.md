# laravel-adminpanel

## Introduction
* This is a laravel admin panel, based on Laravel-vuejs with enhancemenets and many modules pre-made, special for the customer requirements.

## Additional Features
* Laravel 5.7
* Vue Components
* Vuex
* JWT-AUTH
* Built-in Laravel Boilerplate Module Generator,
* Dynamic Menu/Sidebar Builder
* Pages Module
* Blog Module
* FAQ Module
* API Boilerplate
* Mailables
* Responses
* Laravel Mix
* Object based javascript Implementation
